package com.quakearts.test.sms_ussd_simulator;

import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;
import javax.websocket.Session;

@Singleton
public class MessageService {
	private ConcurrentHashMap<String, Session> sessionRegistry = new ConcurrentHashMap<>();
	
	public void registerConnection(String mobileNo, Session session) {
		sessionRegistry.put(mobileNo, session);
	}
	
	public void unregisterConnection(String mobileNo) {
		sessionRegistry.remove(mobileNo);
	}
	
	public Session getSessionFor(String mobileNo) 
			throws DeviceUnconnectedException {
		Session session = sessionRegistry.get(mobileNo);
		if(session!=null)
			return session;
		
		throw new DeviceUnconnectedException();
	}
}
