package com.quakearts.test.sms_ussd_simulator;

import java.util.function.Consumer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ApplicationScoped
@ServerEndpoint(value = "/connection/{mobileNo}",
		decoders = PayloadConverter.class,
		encoders = PayloadConverter.class,
		configurator = CDIServerEndpointConfigurator.class)
public class SMSUSSDEndpoint {
	@Inject
	private MessageService messageService;
	
	@OnOpen
	public void opened(Session session, @PathParam("mobileNo") String mobileNo){
		messageService.registerConnection(mobileNo, session);
		session.getUserProperties().put("mobileNo", mobileNo);
	}
	
	@OnClose
	public void closed(Session session){
		messageService.unregisterConnection(session.getUserProperties()
				.get("mobileNo").toString());
	}
	
	@OnMessage
	public void receivedSMSMessage(Session session, Payload payload) {
		int sequence = payload.getMessage()!=null?payload.getMessage().getSequence():
			payload.getRequest().getSequence();
		
		@SuppressWarnings("unchecked")
		Consumer<String> onComplete = (Consumer<String>) session.getUserProperties().get("SEQ"+sequence);
		onComplete.accept(payload.getAction());
	}
	
}
