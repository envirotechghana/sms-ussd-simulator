package com.quakearts.test.sms_ussd_simulator;

public class Payload {
	private SMSMessage message;
	private PaymentRequest request;
	private String action;

	public SMSMessage getMessage() {
		return message;
	}

	public void setMessage(SMSMessage message) {
		this.message = message;
	}
	
	public PaymentRequest getRequest() {
		return request;
	}

	public void setRequest(PaymentRequest request) {
		this.request = request;
	}

	public Payload withMessageAs(SMSMessage message) {
		setMessage(message);
		return this;
	}
	
	public Payload withRequestAs(PaymentRequest request) {
		setRequest(request);
		return this;
	}

	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
}
