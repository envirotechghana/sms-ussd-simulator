package com.quakearts.test.sms_ussd_simulator;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class DeviceUnconnectedExceptionMapper 
	implements ExceptionMapper<DeviceUnconnectedException> {

	@Override
	public Response toResponse(DeviceUnconnectedException exception) {
		return Response.status(404).build();
	}

}
