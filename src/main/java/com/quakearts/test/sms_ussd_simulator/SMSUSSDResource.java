package com.quakearts.test.sms_ussd_simulator;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.EncodeException;
import javax.websocket.Session;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Singleton
@Path("simulate")
public class SMSUSSDResource {

	@Inject
	private MessageService service;
	
	private AtomicInteger sequence = new AtomicInteger(1);
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("sms")
	public void sendSMS(SMSMessage message, @Suspended AsyncResponse response) 
			throws DeviceUnconnectedException, IOException, EncodeException {
		Session session = service.getSessionFor(message.getMobileNo());
		
		int next = sequence.getAndIncrement();
		message.setSequence(next);
		
		Consumer<String> onComplete = action->response.resume(Response.accepted().build());
		session.getUserProperties().put("SEQ"+next, onComplete);
		session.getBasicRemote().sendObject(new Payload().withMessageAs(message));
		
		response.setTimeout(30, TimeUnit.SECONDS);
		response.setTimeoutHandler(a->a.resume(new DeviceUnconnectedException()));
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("ussd")
	public void sendPaymentRequest(PaymentRequest request, @Suspended AsyncResponse response) 
			throws DeviceUnconnectedException, IOException, EncodeException {
		Session session = service.getSessionFor(request.getMobileNo());
		
		int next = sequence.getAndIncrement();
		request.setSequence(next);
		
		Consumer<String> onComplete = action->{
			if("yes".equals(action))
				response.resume(Response.accepted().build());
			else
				response.resume(Response.status(403).build());
		};
		session.getUserProperties().put("SEQ"+next, onComplete);
		session.getBasicRemote().sendObject(new Payload().withRequestAs(request));
		response.setTimeout(30, TimeUnit.SECONDS);
		response.setTimeoutHandler(a->a.resume(new DeviceUnconnectedException()));
	}
}
