package com.quakearts.test.sms_ussd_simulator;

import javax.enterprise.inject.spi.CDI;

import org.apache.tomcat.websocket.server.DefaultServerEndpointConfigurator;

public class CDIServerEndpointConfigurator extends DefaultServerEndpointConfigurator {
	@Override
	public <T> T getEndpointInstance(Class<T> clazz) throws InstantiationException {
		return CDI.current().select(clazz).get();
	}
}
