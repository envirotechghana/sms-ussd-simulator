package com.quakearts.test.sms_ussd_simulator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class SMSUSSDApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		return new HashSet<>(Arrays.asList(SMSUSSDResource.class, 
				DeviceUnconnectedExceptionMapper.class,
				GeneralExceptionMapper.class));
	}
}
